
PY_SETUP = python3 setup.py 

build:
	$(PY_SETUP) build_ext --inplace
.PHONY: build

install: build
	$(PY_SETUP) install
.PHONY: install

clean:
	$(PY_SETUP) clean
	find . -name \*.so -delete
	rm -rf build
	rm -f mcmc/*.c
	rm -f mcmc/*.html
.PHONY: clean

