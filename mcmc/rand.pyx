
import random

cdef extern from "rand/rand_c_funcs.h":
    void seed_x(size_t s);
    size_t rseed_x();
    size_t rand_x(size_t max);
    double rand_x_0to1();

cpdef double rand():
    return rand_x_0to1()

cpdef size_t rand_int(size_t max):
    return rand_x(max)

cpdef seed(seed):
    global _seed
    seed_x(seed)
    random.seed(seed)
    _seed = seed

cpdef random_seed():
    seed = rseed_x()
    random.seed(seed)
    return seed

cpdef get_seed():
    return  _seed

# just to make sure, the PRNG always gets a proper initial seed
# for a predefined seed, use the function seed()
_seed = random_seed()
