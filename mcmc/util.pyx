

import datetime
import threading

__timers = {}

def add_timer(id, timeout, f, args=None):
    """
    Executes a function f after a given timout in seconds.

    :param id: the timer identifier (so it can be selected for cancellation)
    :param timeout: timeout in seconds
    :param f: the callback function
    :param args: arguments to pass to the function
    """
    global __timers
    cancel_timer(id)
    t = threading.Timer(timeout, f, args)
    t.setDaemon(True)
    t.start()
    __timers[id] = t

def cancel_timer(id):
    """
    cancels a previously started timer

    :param id: the timer identifier to cancel
    """
    global __timers
    if id in __timers:
        __timers[id].cancel()
        del __timers[id]

def cancel_timers():
    global __timers
    [timer.cancel() for timer in __timers.values()]
    __timers = {}

def get_datetime_str():
    return datetime.datetime.now().replace(microsecond=0).isoformat().replace('-', '').replace(':', '').replace('T', '_')

