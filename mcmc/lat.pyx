# cython: profile=True

'''
Created on 30 Sep 2017

@author: db
'''

cimport cython

import math

import numpy as np
cimport numpy as np

cimport rand

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cdef class Navigation:
    @classmethod
    def add_arguments(cls, ap):
        '''
        Appends the command line arguments needed by the class Navigation to work properly

        :param ap: the argument parser to append the arguments
        :return: gives back the argument parser
        '''
        ap.add_argument('--dimensions'
                        , help='the number and size of dimensions as tuple (e.g. (2,3,4) [None]'
                        , default=None
                        , type=tuple)
        return ap

    def __init__(self, args):
        """
        :param args: The command line arguments from the argparse module. It needs the args.dimensions to work properly
        """
        if args.dimensions is None:
            raise RuntimeError('Navigation.__init__(): dimension is invalid (None)')
        self.dimension_n = len(args.dimensions)
        if self.dimension_n < 2:
            raise RuntimeError('Navigation.__init__(): not enough dimension given (< 2)')
        for dim in args.dimensions:
            if not dim:
                raise RuntimeError('Navigation.__init__(): all dimensions must be > 0')
        self.dimensions = args.dimensions

        self._init_length_based_members()
        self._init_neighbors()

    def _init_length_based_members(self):
        self.site_n = self.volume = np.prod(np.array(self.dimensions))
        if self.site_n < 4:
            raise RuntimeError('Navigation._init_length_based_members: minimum number of sites is 4!')
        self.direction_n = 2 * self.dimension_n

    def _init_neighbors(self):
        self.neighbors = np.empty((self.site_n, self.direction_n), dtype=np.uint)
        cdef size_t site_id, direction_id
        self._coordinates_cache = {}
        for site_id in range(self.site_n):
            for direction_id in range(self.direction_n):
                self.neighbors[site_id, direction_id] = self._get_neighbor_calc(site_id, direction_id)

    def _get_neighbor_calc(self, size_t site_id, size_t direction_id):
        shift = 1
        if direction_id >= self.dimension_n:
            direction_id -= self.dimension_n
            shift = -1
        coordinates = list(self.site_id_2_coordinates(site_id))
        coordinates[direction_id] = (coordinates[direction_id] + shift)%self.dimensions[direction_id]
        return self.coordinates_2_site_id(tuple(coordinates))

    cpdef np.ndarray get_neighbors(self, size_t site_id):
        return self.neighbors[site_id]

    cpdef size_t get_neighbor(self, size_t site_id, size_t direction_id):
        return self.neighbors[site_id, direction_id]

    cpdef size_t reverse_direction(self, size_t direction_id):
        return (direction_id + self.dimension_n)%self.direction_n

    cpdef tuple site_id_2_coordinates(self, size_t site_id):
        try:
            return self._coordinates_cache[site_id]
        except KeyError:
            return self.__site_id_2_coordinates(site_id)

    def  __site_id_2_coordinates(self, size_t site_id):
        cdef list coordinates = []
        cdef size_t d, c, d_jump = np.prod(np.array(self.dimensions))
        for d in self.dimensions[::-1]:
            d_jump /= d
            c = site_id // d_jump
            coordinates.append(c)
            site_id -= c*d_jump
        coordinates.reverse()
        cdef tuple coords = tuple(coordinates)
        self._coordinates_cache[site_id] = coords
        self._coordinates_cache[coords] = site_id
        return coords

    cpdef size_t coordinates_2_site_id(self, tuple coordinates):
        try:
            return self._coordinates_cache[coordinates]
        except KeyError:
            return self.__coordinates_to_site_id(coordinates)

    def __coordinates_to_site_id(self, tuple coordinates):
        cdef size_t site_id = 0
        cdef size_t d_jump = 1
        for i in range(self.dimension_n):
            site_id += coordinates[i]*d_jump
            d_jump *= self.dimensions[i]
        self._coordinates_cache[coordinates] = site_id
        self._coordinates_cache[site_id] = coordinates
        return site_id

    cpdef size_t rand_site(self):
        return rand.rand_int(self.site_n)

    cpdef size_t rand_neighbor(self, size_t site_id):
        cdef size_t direction_id = self.rand_direction()
        return self.neighbors[site_id, direction_id]

    cpdef size_t rand_direction(self):
        return rand.rand_int(self.direction_n)

    cpdef double distance(self, size_t site_a, size_t site_b):
        if site_a == site_b:
            return 0.0
        cdef list a, b
        cdef size_t d2
        a = self.site_id_2_coordinates(site_a)
        b = self.site_id_2_coordinates(site_b)
        d2 = sum([(a[i] - b[i])**2 for i in range(self.dimension_n)])
        return math.sqrt(d2)
