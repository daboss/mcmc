
#include <stdint.h>

void seed_x(uint64_t);
uint64_t rseed_x(void);
uint64_t rand_x(uint64_t);
double rand_x_0to1(void);
