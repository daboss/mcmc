# cython: profile=True

'''
Created on 30 Sep 2017

@author: db
'''

cimport numpy as np


cdef class Navigation:
    """
    The Navigation class can be used to navigate through the neighbors of an euclidean n-dimensional lattice.

    The sites of the lattice are identified by an index, independent of the number of dimensions of the lattice

    """
    cdef readonly:
        tuple dimensions # the length of the dimensions e.g. (4,4,4) is a 3-d lattice with side length 4
        size_t dimension_n # number of dimensions of the lattice
        size_t direction_n # number of directions of the lattice (= 2 * dimension_n)
        size_t volume, site_n # the number of sites of the lattice (aka volume)
        size_t[:,::1] neighbors # array containing the (cached) neighbors of a site
    cdef:
        dict _coordinates_cache #  cache for storing the coordinates for a given site id (and vice versa)

    cpdef np.ndarray get_neighbors(self, size_t site_id)
    """
    returns the neighbors of the site

    :param site_id: the index of the site of interest
    :returns: the array of neighbors of the site
    """

    cpdef size_t get_neighbor(self, size_t site_id, size_t direction_id)
    """
    returns the neighbor in the given direction

    :param site_id: the origin site
    :param direction_id: the direction (direction 0 to (dimension_n -1) are positive directions, dimension_n to 
    2*dimension_n -1 are the negative directions
    :returns: the site index of the neighbor of site x in direction y
    """

    cpdef size_t reverse_direction(self, size_t direction_id)
    """
    gets the reverse/opposite direction of a given direction

    :param direction_id: the direction (direction 0 to (dimension_n -1) are positive directions, dimension_n to 
    2*dimension_n -1 are the negative directions
    :returns: the opposite direction of the input direction
    """

    cpdef size_t rand_site(self)
    """
    selects a random site of the lattice
    
    :returns: a random site index of the lattice
    """

    cpdef size_t rand_neighbor(self, size_t site_id)
    """
    selects a random neighbor of the lattice site given

    :param site: the index of the site of interest 
    :returns: the site index of a random neighbor of the given site
    """

    cpdef size_t rand_direction(self)
    """
    selects a random direction of the lattice
    
    :returns: a random direction index
    """

    cpdef tuple site_id_2_coordinates(self, size_t site_id)
    """
    translates a given site index into euclidean coordinates

    :param site_id: the index of the site of interest 
    :returns: the euclidean coordinates for the site index given
    """

    cpdef size_t coordinates_2_site_id(self, tuple coordinates)
    """
    translates coordinates into a site index

    :param coordinates: the euclidean coordinates of a site
    :returns: the site index of the coordinates given 
    """

    cpdef double distance(self, size_t site_id_a, size_t site_id_b)
    """
    calculates the euclidean distance between two site indices

    :param site_id_a: index of site a
    :param site_id_b: index of site b
    :returns: the distance in unit lattice spacings
    """
