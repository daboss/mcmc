

cimport rec

cdef class Simulation:
    cdef readonly :
        rec.Recorder recorder,
        double size
    cdef:
        args
        bint _debug

    cpdef void step(self)

cdef class Simulator:
    cdef:
        args
        bint _save_tmp_results_now, _do_stop
        size_t _step_n
        double _t_simulation, _t_step, _t_measure
        Simulation _simulation

    cpdef void run(self)

    cdef void _check_stop_conditions(self)
    cdef void _step(self)
    cdef void _loop(self)

