# cython: profile=True

import math
import socket
import statistics
import sys
import time

import pandas as pd

cimport rand
import util


FMT_PAR = '{}.par'
FMT_MEAN = '{}.mean'
FMT_BOOTSTRAP = '{}.sd_bootstrap'
FMT_SD = '{}.sd'


cpdef double sd_bootstrap(values, n=100):
    '''
    Calculates the standard deviation of bootstrapped samples from values

    :param values: a list of values
    :param n: number of samples to do in bootstrap
    :return: the standard deviation of the values
    '''
    try:
        samples = bootstrap_sample(values, n)
        if samples:
            return statistics.stdev(samples)
        else:
            return math.nan
    except statistics.StatisticsError:
        return math.nan


cpdef list bootstrap_sample(list values, int n=100):
    '''
    Creates a list of bootstrap sampled mean values.
    :param values: The individual original values
    :param n: The number of bootstrap samples to generate
    :return: the list of the bootstrap sampled mean values
    '''
    l = len(values)
    try:
        return [statistics.mean(
                [values[rand.rand_int(l)] for j in range(l)]
        ) for i in range(n)]
    except statistics.StatisticsError:
        return None


cdef class StateRecorder:
    cdef readonly:
        measure_f
        _measurements_callback
        size_t block_n, measurement_n
        dict derived_observables
        dict observables
        dict observable_args
    cdef:
        args
        dict block_measurements
        dict measurements
        dict normalisers
        dict parameters
        size_t block_size
        str state
        int start_time

    def __init__(self, args, state):
        self.args = args
        self.state = state
        self.block_size = args.block_size
        if self.block_size > 1:
            self.measure_f = self._measure_block
        else:
            self.measure_f = self._measure_direct
        self.block_measurements = {}
        self.block_n = self.measurement_n = 0
        self.measurements = {}
        self.normalisers = {}
        self.observable_args = {}
        self.observables = {}
        self.derived_observables = {}
        self.parameters = {}
        self._measurements_callback = self._all_measurements

        self.add_parameter('state', state)
        self.add_parameter('identifier', args.identifier)
        if args.series:
            self.add_parameter('series', args.series)
        self.add_parameter('start_time', util.get_datetime_str())

        self.start_time = int(time.time())

    def add_observable(self, observable, obs_name=None, args=None, normaliser=1.0):
        if not obs_name:
            obs_name = observable.__name__
        if args:
            self.observable_args[obs_name] = args
        self.observables[obs_name] = observable
        self.measurements[obs_name] = []
        self.normalisers[obs_name] = normaliser
        if self.args.block_size > 1:
            self.block_measurements[obs_name] = []

    def add_observable_derived(self, observable, obs_name=None, args=None, normaliser=1.0):
        if not obs_name:
            obs_name = observable.__name__
        if args:
            self.observable_args[obs_name] = args
        self.derived_observables[obs_name] = observable
        self.normalisers[obs_name] = normaliser

    def add_parameter(self, parameter, value, normaliser=1.0):
        self.parameters[parameter] = value
        self.measurements[parameter] = []
        self.normalisers[parameter] = normaliser

    cdef void _measure_direct(self):
        self.measurement_n += 1
        self._append_measurements(self.measurements)
        [self.measurements[par].append(value) for par, value in self.parameters.items()]
        self.block_n += 1

    cdef void _append_measurements(self, dict measurements):
        [measurements[obs].append(f_obs() * self.normalisers[obs])
         for obs, f_obs in self.observables.items() if obs not in self.observable_args]
        [measurements[obs].append(f_obs(obs, self.observable_args[obs]) * self.normalisers[obs])
         for obs, f_obs in self.observables.items() if obs in self.observable_args]

    cdef void _measure_block(self):
        self.measurement_n += 1
        self._append_measurements(self.block_measurements)
        if self.measurement_n % self.block_size == 0:
            for obs in self.observables.keys():
                self.measurements[obs].append(sum(self.block_measurements[obs])/self.args.block_size)
                self.block_measurements[obs] = []
            [self.measurements[par].append(value) for par, value in self.parameters.items()]
            self.block_n += 1

    @property
    def results(self):
        cdef dict results = {FMT_PAR.format(par): value for par, value in self.parameters.items()}
        results[FMT_PAR.format('n')] = self.measurement_n
        results[FMT_PAR.format('run_time')] = int(time.time()) - self.start_time
        results[FMT_PAR.format('host')] = socket.gethostname()
        [self._append_results(results, obs, data) for obs, data in self.measurements.items() if obs in self.observables]
        [self._append_results_derived(results, obs) for obs in self.derived_observables]
        return results

    cdef void _append_results_derived(self, dict results, str obs):
        results[FMT_MEAN.format(obs)]      = self.mean_derived(obs)
        results[FMT_BOOTSTRAP.format(obs)] = self.sd_bootstrap_derived(obs)

    cdef void _append_results(self, dict results, str obs, list data):
        results[FMT_MEAN.format(obs)]      = self._result(statistics.mean, data)
        results[FMT_BOOTSTRAP.format(obs)] = self._result(sd_bootstrap, data)
        results[FMT_SD.format(obs)]        = self._result(statistics.stdev, data)

    def _result(self, f, list data):
        try: return f(data)
        except statistics.StatisticsError: return math.nan

    def mean(self, obs):
        return statistics.mean(self.measurements[obs])

    def mean_derived(self, obs):
        if obs in self.observable_args:
            return self.derived_observables[obs](obs, self.observable_args[obs])
        else:
            return self.derived_observables[obs]()

    def sd_bootstrap(self, obs):
        return sd_bootstrap(self.measurements[obs])

    def sd_bootstrap_derived(self, obs):
        self._measurements_callback = self.bootstrap_sample
        if obs in self.observable_args:
            bootstrap = [self.derived_observables[obs](obs, self.observable_args[obs]) for i in range(100)]
        else:
            bootstrap = [self.derived_observables[obs]() for i in range(100)]
        self._measurements_callback = self._all_measurements
        return statistics.stdev(bootstrap)

    def bootstrap_sample(self, observables):
        n = self.block_n
        sample = {}
        for obs in self.observables.keys():
            sample[obs] = []
        for i in range(n):
            r = rand.rand_int(n)
            for obs in observables:
                sample[obs].append(self.measurements[obs][r])
        return sample

    def _all_measurements(self, observables):
        return self.measurements

    def measurements_callback(self, observables):
        return self._measurements_callback(observables)

    def __str__(self):
        strings = []
        strings.append('Parameters ({})'.format(self.state))
        for par in sorted(self.parameters):
            try:
                strings.append('    \033[1m{:24s} = {:.3e}\033[0m'.format(par[:24], self.parameters[par]))
            except ValueError:
                strings.append('    \033[1m{:24s} = {}\033[0m'.format(par[:24], self.parameters[par]))
        res = self.results
        strings.append('    \033[1m{:24s} = {:.3e}\033[0m'.format('n', res['n.par']))
        strings.append('    \033[1m{:24s} = {:.3e}\033[0m'.format('run_time', res['run_time.par']))
        observables = []
        observables += self.observables.keys()
        observables += self.derived_observables.keys()
        if observables:
            strings.append('Observables ({})'.format(self.state))
            for obs in sorted(observables):
                # for later reference: strings.append('\033[93m\033[1m{:24s} = {:.2e}\033[0m +/- {:.1e}    [{:.2e},
                # {:.2e}]\033[0m'
                strings.append('    \033[1m{:24s} = {:.3e}\033[0m +/- {:.1e}'
                               .format(obs[:24], res[FMT_MEAN.format(obs)], res[FMT_BOOTSTRAP.format(obs)]))
        return '\n'.join(strings)

    def save_results(self, path, file_identifier, prefix=None, suffix=None, is_final=False):
        if not prefix:
            prefix = 'r_'
        if not suffix:
            suffix = 'csv'
        filename = '{}/{}{}-state={}.{}'.format(path, prefix, file_identifier, self.state, suffix)
        results = self.results
        df = pd.DataFrame([results], columns=results.keys())
        df.T.to_csv(filename)
        if is_final:
            sys.stdout.write('\nwrote result file {}'.format(filename))
            sys.stdout.flush()

    def save_measurements(self, path, file_identifier, prefix=None, suffix=None, compression=False):
        if not prefix:
            prefix = 'm_'
        if not suffix:
            if compression:
                suffix = 'csv.gzip'
            else:
                suffix = 'csv'
        filename = '{}/{}{}.state={}.{}'.format(path, prefix, file_identifier, self.state, suffix)
        if compression:
            pd.DataFrame(self.measurements).to_csv(filename, compression='gzip', index=False)
        else:
            pd.DataFrame(self.measurements).T.to_csv(filename, index=False)
        sys.stdout.write('\nwrote measurement file {}'.format(filename))
        sys.stdout.flush()

STATE_ALL = 'all'

cdef class Recorder:
    '''
    The recorder is used to take and store the measurments during the simulation run.
    '''
    @classmethod
    def add_arguments(cls, ap):
        ap.add_argument('--block_size'
                        , help='number of individual measurements to be combined into a block [1000]'
                        , default=1000
                        , type=int)
        ap.add_argument('--p_state'
                        , help='the state to check precision [{}]'.format(STATE_ALL)
                        , default=STATE_ALL
                        , type=str)
        ap.add_argument('--series'
                        , help='The measurement series identifier [None]'
                        , default=None
                        , type=str)
        return PrecisionCheck.add_arguments(ap)

    def __init__(self, args):
        self.args = args

        self.state = STATE_ALL
        self.states = {STATE_ALL: StateRecorder(args, STATE_ALL)}
        self.measurement_n = 0
        self._prepare_measure_functions = []
        self._precision_check = None

    def set_p_observable(self, observable_name, state=STATE_ALL):
        '''
        Sets the precision observable - the observable upon which the precision measurements are done.

        :param observable_name:  the name of the observable as introduced with the @add_observable function
        :param state: the (simulation) state the observable is measured in
        :return:
        '''
        self.args.p_observable = observable_name
        self.args.p_state = state

    def add_prepare_measure_function(self, f):
        '''
        Can be used in case there are some preparation steps needed before doing the measurement.
        Can also be used to speed up the measurement, in case some temporary data can be built
        from which various observables can profit.

        :param f: the function to call before the measurements are done.
        :return: nothing
        '''
        self._prepare_measure_functions.append(f)

    def add_observable(self, observable, obs_name=None, args=None, normaliser=1.0, state=None):
        '''
        Adds a (direct) observable to be recorded.

        :param observable: the callback function for the observable to be measured
        :param obs_name: the name under which des observable is stored (if different from function name)
        :param args: arguments to pass back to the
        :param normaliser: when doing the measurement, the measured value is multiplied by this value.
            Can be used for example to normalise the observables by the volume (normaliser = 1/V)
        :param state: the (simulation) state the observable is measured
        :return: nothing
        '''
        if not state:
            state = STATE_ALL
        if state not in self.states:
            self.states[state] = StateRecorder(self.args, state)
        self.states[state].add_observable(observable, obs_name, args, normaliser)

    def add_observable_derived(self, observable, obs_name=None, args=None, normaliser=1.0, state=None):
        '''
        Adds a derived observable to be recorded, an observable, that is calculated from various direct observables.

        :param observable: the callback function for the observable to be measured
        :param obs_name: the name under which des observable is stored (if different from function name)
        :param args: arguments to pass back to the
        :param normaliser: when doing the measurement, the measured value is multiplied by this value.
            Can be used for example to normalise the observables by the volume (normaliser = 1/V)
        :param state: the (simulation) state the observable is measured
        :return: nothing
        '''
        if not state:
            state = STATE_ALL
        if state not in self.states:
            self.states[state] = StateRecorder(self.args, state)
        self.states[state].add_observable_derived(observable, obs_name, args, normaliser)

    def add_parameter(self, parameter, value, normaliser=1.0, state=None):
        if not state:
            state = STATE_ALL
        if state not in self.states:
            self.states[state] = StateRecorder(self.args, state)
        self.states[state].add_parameter(parameter, value, normaliser)

    cpdef void measure(self):
        '''
        retrieves the values of all observables and stores them.
        
        :return: nothing
        '''
        [f() for f in self._prepare_measure_functions]

        if self.state != STATE_ALL:
            self.states[STATE_ALL].measure_f(self.states[STATE_ALL])
        try:
            self.states[self.state].measure_f(self.states[self.state])
        except KeyError:
            pass
        self.measurement_n += 1

    def measurements_callback(self, obs):
        return self.states[STATE_ALL].measurements_callback(obs)

    def is_precision_ok(self):
        if self.args.p_block_count_min > self.states[self.args.p_state].block_n:
            return False
        if not self._precision_check:
            self._precision_check = PrecisionCheck.factory(self.states[self.args.p_state], self.args)
        return self._precision_check.is_precision_ok()

    def __str__(self):
        strings = []
        for state in self.states:
            strings.append(self.states[state].__str__())
            strings.append('')
        return '\n'.join(strings)

    def save_results(self, path, file_identifier, prefix=None, suffix=None, is_final=False):
        if not path:
            raise RuntimeError("Recorder.save_results: no path given")
        if not file_identifier:
            raise RuntimeError("Recorder.save_results: no file identifier given")
        [state.save_results(path, file_identifier, prefix, suffix, is_final)
         for state in self.states.values()]

    def save_measurements(self, path, file_identifier, prefix=None, suffix=None, compression=False):
        if not path:
            raise RuntimeError("Recorder.save_measurements: no path given")
        if not file_identifier:
            raise RuntimeError("Recorder.save_measurements: no file identifier given")
        [state.save_measurements(path, file_identifier, prefix, suffix, compression)
         for state in self.states.values()]


class PrecisionCheck:
    @classmethod
    def add_arguments(cls, ap):
        ap.add_argument('--p_block_count_min'
                        , help='Minimum number of blocks recorded, before first precision check is done [100]'
                        , default=100
                        , type=int)
        ap.add_argument('--p_absolute'
                        , help='the absolute precision (standard deviation) of the simulation run to attain [0.0]'
                        , default=0.0
                        , type=float)
        ap.add_argument('--p_relative'
                        , help='the relative precision (standard deviation) of the simulation run to attain [0.0]'
                        , default=0.0
                        , type=float)
        ap.add_argument('--p_xover_absolute_to_relative'
                        , help='Mean value threshold, at which the precision check switches from absolute to relative '
                               'standard deviation [None]'
                        , default=None
                        , type=float)
        ap.add_argument('--p_observable'
                        , help='the observable to check precision [None]'
                        , default=None
                        , type=str)
        ap.add_argument('--p_repetition_n'
                        , help='Number of consecutive precision checks that must be below the required precision to '
                               'stop the simulation [2]'
                        , default=2
                        , type=int)
        ap.add_argument('--p_interval'
                        , help='interval in hours between precision measurements [0.0] (= no precision measurement)'
                        , default=0.0
                        , type=float)
        return ap

    @classmethod
    def factory(cls, recorder, args):
        if not args.p_observable:
            return NoPrecisionCheck(recorder, args)
        if args.p_interval < 0.001:
            return NoPrecisionCheck(recorder, args)
        if args.p_absolute <= 0 and args.p_relative <= 0:
            return NoPrecisionCheck(recorder, args)
        # first check in derived observables
        if args.p_observable in recorder.derived_observables:
            return DerivedPrecisionCheck(recorder, args)
        elif args.p_observable in recorder.observables:
            return PrecisionCheck(recorder, args)
        else:
            raise ValueError('unknown observable \'{}\''.format(args.p_observable))

    def __init__(self, recorder, args):
        self._args = args
        self._recorder = recorder
        self._observable = args.p_observable
        self._interval_in_s = args.p_interval * 3600
        self._xover_absolute_to_relative = args.p_xover_absolute_to_relative
        self._repetition_n = args.p_repetition_n
        self._precision_ok_n = 0
        self._check_precision_now = True
        if self._xover_absolute_to_relative is not None:
            if args.p_absolute <= 0 or args.p_relative <= 0:
                raise ValueError('p_absolute and p_relative must be > 0 for mixed absolute checking')
            self.f_sd = self.sd_absolute_or_relative
        elif args.p_relative > 0:
            self._precision = args.p_relative
            self.f_sd = self.sd_relative
        else:
            self._precision = args.p_absolute
            self.f_sd = self.sd

    def is_precision_ok(self):
        if self._check_precision_now:
            self._check_precision()
        return bool(self._precision_ok_n >= self._repetition_n)

    def _check_precision(self):
        sd = self.f_sd()

        sys.stdout.write('\n{} = {:.2e}\n'.format(FMT_BOOTSTRAP.format(self._observable), sd))
        sys.stdout.flush()
        if math.isnan(sd) or sd > self._precision:
            self._precision_ok_n = 0
        else:
            self._precision_ok_n += 1
        self._check_precision_now = False
        if self._interval_in_s > 0:
            util.add_timer('precision', int(self._interval_in_s), self.enable_check_precision_now)

    def sd_relative(self, mean=None):
        """
        Calculates the standard deviation relative to the mean value

        :param mean: the mean value of the observable
        :return: the relative standard deviation of the precision observable
        """
        if mean is None:
            mean = self.mean()
        mean = abs(mean)
        if mean > 0:
            return self.sd()/mean
        else:
            return math.inf

    def sd(self):
        """
        :return: the (bootstrapped) standard deviation of the precision observable
        """
        return self._recorder.sd_bootstrap(self._observable)

    def mean(self):
        """
        :return: the mean value of the precision observable
        """
        return self._recorder.mean(self._observable)

    def sd_absolute_or_relative(self):
        """
        Calculates either the absolute standard deviation or the relative one, depending on the mean value in
        relation to the crossover value
        :return: either the absolute or the relative standard deviation
        """
        mean = self.mean()
        if mean >= -self._xover_absolute_to_relative and mean <= self._xover_absolute_to_relative:
            self._precision = self._args.p_absolute
            return self.sd()
        else:
            self._precision = self._args.p_relative
            return self.sd_relative(mean)

    def enable_check_precision_now(self):
        self._check_precision_now = True

class NoPrecisionCheck(PrecisionCheck):
    def is_precision_ok(self):
        return False
    def _check_precision(self):
        pass

class DerivedPrecisionCheck(PrecisionCheck):
    def mean(self):
        return self._recorder.mean_derived(self._observable)
    def sd(self):
        return self._recorder.sd_bootstrap_derived(self._observable)


