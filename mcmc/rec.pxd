

cdef class Recorder:
    cdef readonly :
        size_t measurement_n
    cdef:
        args
        _measurements_callback
        _precision_check
        size_t _block_count
        str state
        dict states
        list _prepare_measure_functions


    cpdef void measure(self)

