
cpdef double rand()
cpdef size_t rand_int(size_t max)

cpdef seed(seed)
cpdef random_seed()

cpdef get_seed()

