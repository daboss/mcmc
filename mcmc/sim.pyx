# cython: profile=True

cimport cython
import sys
cdef bint is_interactive = sys.stdout.isatty()
if is_interactive:
    from cpython.exc cimport PyErr_CheckSignals

import faulthandler
faulthandler.enable()

import argparse
import datetime
import os
import signal
import time

from pathlib import Path

cimport rand
cimport rec
import util

__do_stop = False

def sigint_handler(signal, frame):
    global __do_stop
    sys.stdout.write('\ngot SIGINT\n')
    sys.stdout.flush()
    __do_stop = True

def print_args(args):
    for key, value in sorted(vars(args).items()):
        if value is not None:
            sys.stdout.write('\n--{}={} \\'.format(key, value))
    sys.stdout.flush()


cdef class Simulation:
    '''
    Base class for simulations.
    '''
    @classmethod
    def add_arguments(cls, ap=None):
        if not ap: ap = argparse.ArgumentParser()
        ap.add_argument('--debug'
                        , help='enable additional tests and debugging output [False]'
                        , action='store_true')
        ap.add_argument('--folder'
                        , help='the (base) folder to store the measurements, defaults to current folder [{}/]'.format(Path.cwd())
                        , default='{}/'.format(Path.cwd())
                        , type=str)
        ap.add_argument('--identifier'
                        , help='the identifier of the simulation run [ISO date]'
                        , default=util.get_datetime_str()
                        , type=str)
        ap.add_argument('--sample_rate'
                        , help='the number of steps/sweeps before doing a measurement [1]'
                        , default=1
                        , type=int)
        ap.add_argument('--seed'
                        , help='PRNG seed, None = random seed [None]'
                        , default=None
                        , type=int)
        ap.add_argument('--steps'
                        , help='the number of steps to do before ending the simulation [0=disabled]'
                        , default=0
                        , type=int)
        ap.add_argument('--t_steps'
                        , help='thermalizing steps: the number of steps to do before starting the measurements [0]'
                        , default=0
                        , type=int)
        ap.add_argument('--tmp_results_interval'
                        , help='interval to write out temporary results in hours [None]'
                        , default=None
                        , type=float)
        ap.add_argument('--timeout'
                        , help='after how many hours should the simulation stop latest? [0=disabled]'
                        , default=0
                        , type=float)
        rec.Recorder.add_arguments(ap)
        return ap

    def __init__(self, args, size=1):
        self.args = args
        self.size = size
        self._debug = args.debug
        self.recorder = rec.Recorder(args)

    cpdef void step(self):
        raise NotImplementedError

    def get_path(self):
        path = '{}/{}/{}/'.format(os.path.abspath(self.args.folder), self.get_param_str_short(), self.args.identifier)
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except FileExistsError:
                # should not happen but it does anyway
                pass
        return path

    def get_file_identifier(self):
        return '{}-id={}'.format(self.get_param_str_full(), self.args.identifier)

    def _get_param_str_short(self):
        raise RuntimeError('Simulator._get_param_str_short(): Simulator is abstract!')

    def _get_param_str_full(self):
        raise RuntimeError('Simulator._get_param_str_full(): Simulator is abstract!')


cdef class Simulator:
    '''
    Runs the actual simulation and does 
    '''
    def __init__(self, args, simulation):
        if not args.timeout and not args.steps:
            raise RuntimeError('Simulator.__init__: no stopping condition set, either steps or timeout must be set')
        self.args = args
        if args.seed:
            rand.seed(args.seed)
        else:
            sys.stdout.write('\n--seed={} \\'.format(rand.get_seed()))
            sys.stdout.flush()

        if not simulation:
            raise RuntimeError('Simulator.__init__: no simulation to run given')
        self._simulation = simulation

        self._simulation.recorder.add_parameter('command', sys.argv[0])
        self._simulation.recorder.add_parameter('args', ' '.join(sys.argv[1:]))
        self._simulation.recorder.add_parameter('identifier', args.identifier)

        self._simulation.recorder.add_observable(self.t_step, normaliser=1.0/self._simulation.size)
        self._simulation.recorder.add_observable(self.t_measure, normaliser=1.0/self._simulation.size)

        self._step_n = 0
        self._t_step = 0.0
        self._t_measure = 0.0
        self._t_simulation = 0.0

        self._save_tmp_results_now = False
        if is_interactive:
            signal.signal(signal.SIGINT, sigint_handler)

    cpdef void run(self):
        if self.args.timeout:
            self._start_timeout_timer()
        [self._simulation.step() for i in range(self.args.t_steps)]
        if self.args.tmp_results_interval:
            self._start_tmp_results_timer()
        t_start = time.process_time()
        self._loop()
        self._t_simulation = time.process_time() - t_start

        util.cancel_timers()

    def _start_timeout_timer(self):
        global __do_stop
        __do_stop = False
        util.add_timer('timeout', int(self.args.timeout*3600), self.timeout)

    def timeout(self):
        self.stop()
        sys.stdout.write('\ntimeout reached\n')
        sys.stdout.flush()

    def stop(self):
        global __do_stop
        __do_stop = True

    def _start_tmp_results_timer(self):
        self._save_tmp_results_now = False
        util.add_timer('tmp_results', int(self.args.tmp_results_interval*3600), self._enable_tmp_results)

    def _enable_tmp_results(self):
        self._save_tmp_results_now = True

    cdef void _loop(self):
        global __do_stop
        cdef int i = 1
        sys.stdout.write('\n\nstart @ {} ...\n\n'.format(datetime.datetime.now().isoformat()))
        sys.stdout.flush()
        while not __do_stop:
            self._step()
            time.sleep(0) # releases gil (global interpreter lock) - needed to allow timer events to change variables
            if is_interactive:
                sys.stdout.write('\rstep {} ({:.1e} s)'.format(i, self._t_step))
                sys.stdout.flush()
                i += 1
                PyErr_CheckSignals()
            self._check_stop_conditions()
            if self._save_tmp_results_now and not __do_stop:
                self.save_results()
                sys.stdout.write('\nwrote temp results @ {}\n'.format(datetime.datetime.now().isoformat()))
                sys.stdout.flush()
                self._start_tmp_results_timer()
        sys.stdout.write('\n... stop  @ {}\n'.format(datetime.datetime.now().isoformat()))
        sys.stdout.flush()

    cdef void _step(self):
        cdef double t = time.process_time()
        self._simulation.step()
        self._t_step = time.process_time() - t

        self._step_n += 1

        t = time.process_time()
        if self._step_n%self.args.sample_rate == 0:
            self._simulation.recorder.measure()
        self._t_measure = time.process_time() - t

    cdef void _check_stop_conditions(self):
        if self.args.steps and self._step_n >= self.args.steps:
            self.stop()
            sys.stdout.write('\nsteps reached\n')
            sys.stdout.flush()
        elif self._simulation.recorder.is_precision_ok():
            self.stop()
            sys.stdout.write('\nprecision reached\n')
            sys.stdout.flush()

    def t_step(self):
        return self._t_step

    def t_measure(self):
        return self._t_measure

    def step_n(self):
        return self._step_n

    def save_results(self, path=None, file_identifier=None, is_final=False):
        if path is None:
            path = self._simulation.get_path()
        if file_identifier is None:
            file_identifier = self._simulation.get_file_identifier()
        prefix = None if is_final else 'r_tmp_'
        self._simulation.recorder.save_results(path, file_identifier, prefix=prefix)

    def save_measurements(self, path=None, file_identifier=None, compression=False):
        if path is None:
            path = self._simulation.get_path()
        if file_identifier is None:
            file_identifier = self._simulation.get_file_identifier()
        self._simulation.recorder.save_measurements(path, file_identifier, compression=compression)

    def __str__(self):
        strings = []
        strings.append('Runtime data:')
        strings.append('\033[1m{:24s} = {}\033[0m'.format('samples', self._simulation.recorder.measurement_n))
        strings.append('\033[1m{:24s} = {} ({:.1f} s) \033[0m'.format('cpu runtime',
            time.strftime('%H:%M:%S', time.gmtime(self._t_simulation)), self._t_simulation))
        strings.append('')
        strings.append(self._simulation.recorder.__str__())
        return '\n'.join(strings)
