
import argparse
import unittest

import mcmc.lat as lat


class NavigationTest(unittest.TestCase):

    def test_init_nok(self):
        for dims in [None, (0,2), (1,2), (1, 1, 1, 1), (0, 4, 4)]:
            with self.assertRaises(RuntimeError) as context:
                self.get_nav(dims)
            self.assertTrue(str(context.exception))

    def test_init_ok(self):
        for dims in [(2,2), (2,2,2), (3,3,3), (2,2,2,2), (2,2,2,2,2), (2,2,2,2,2,2,2,2,2)]:
            self.check_navigation(dims)
        for dims in [(2,3), (2,3,2), (2,3,2,3), (2,3,2,3,2), (2,3,2,3,2,3,2,3,2)]:
            self.check_navigation(dims)

    def check_navigation(self, dimensions):
        print(dimensions)
        nav = self.get_nav(dimensions)
        site_n = 1
        for d in dimensions:
            site_n *= d
        self.assertEqual(site_n, nav.site_n)
        for site in range(nav.site_n):
            self.assertEqual(site, nav.site_4_coordinates(nav.coordinates_4_site(site)))
        for dir in range(nav.direction_n):
            self.assertEqual(dir, nav.reverse_direction(nav.reverse_direction(dir)))
        for site in range(nav.site_n):
            for dir in range(nav.direction_n):
                neighbor = nav.get_neighbor(site, dir)
                rev_dir = nav.reverse_direction(dir)
                self.assertEqual(site, nav.get_neighbor(neighbor, rev_dir))

    def get_nav(self, dimensions):
        ap = argparse.ArgumentParser()
        ap = lat.Navigation.add_arguments(ap)
        args = ap.parse_args()
        args.dimensions = dimensions
        return lat.Navigation(args)

    @unittest.skip
    def test_fail(self):
        self.assertTrue(False)

