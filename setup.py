from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize

import os
import numpy

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
        return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = 'mcmc',
    version = '0.0.1',
    author = 'Dan Boss',
    author_email = 'dan.boss.x@gmail.com',
    description = ('A Markov chain Monte Carlo simulator engine'),
    license = 'MIT',
    keywords = 'cython montecarlo simulation lattice qcd',
    url = 'https://gitlab.com/daboss/mcmc',
    packages=['mcmc'],
    package_data={'mcmc': ['*.pxd']},
    long_description=read('README.md'),
    classifiers=[
    'Development Status :: 3 - Alpha',
    'Environment :: Console',
    'Intended Audience :: Science/Research',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Cython',
    'Topic :: Scientific/Engineering :: Physics',
    ],
)
extensions = [
	Extension('*',
		['mcmc/*.pyx', 'mcmc/rand/rand_c_funcs.c'],
                extra_compile_args=['-std=gnu99'],
                include_dirs=[numpy.get_include()],
	),
]
setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = cythonize(extensions, annotate=True),
    include_dirs=[numpy.get_include()],
)
